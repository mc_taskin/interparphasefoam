# interParPhaseFoam

Multiphase OpenFOAM solver created for my master thesis. Essentially created for simulating dehmidification process in spray type direct contact condenser. Still under development, the detailed documentation will be created after development and validation of the solver.